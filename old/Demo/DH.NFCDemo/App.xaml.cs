﻿namespace DH.NFCDemo
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // 强光主题
            UserAppTheme = AppTheme.Light;

            //MainPage = new AppShell();
            MainPage = new MainPage();
        }
    }
}