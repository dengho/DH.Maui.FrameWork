﻿using System.ComponentModel;
using System.Xml.Linq;

namespace GeTuiPushDemo.ViewModels;

public class MainViewModel : BaseViewModel
{
    private string name;

    public string Name
    {
        get => name;
        set => SetProperty(ref name, value);
    }

}
