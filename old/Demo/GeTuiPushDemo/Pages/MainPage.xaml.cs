﻿using GeTuiPushDemo.Pages;
using GeTuiPushDemo.ViewModels;

namespace GeTuiPushDemo.Pages;

public partial class MainPage : BasePage
{
    int count = 0;

    private readonly MainViewModel mainvm;

    public MainPage(MainViewModel mainvm) : base(mainvm)
    {
        InitializeComponent();
        this.mainvm = mainvm;

        //Test.Text = this.mainvm.Name;

        //SemanticScreenReader.Announce(Test.Text);
    }

    public const string MIME_TYPE = "application/com.denghao.dhnfc";

    private void OnCounterClicked(object sender, EventArgs e)
    {
        count++;

        if (count == 1)
            CounterBtn.Text = $"Clicked {count} time";
        else
            CounterBtn.Text = $"Clicked {count} times";

        SemanticScreenReader.Announce(CounterBtn.Text);

        Test.Text = "测试";

        SemanticScreenReader.Announce(Test.Text);

        //this.mainvm.Name = "测试1";
        this.mainvm.Name = CommonField.CId + "---" + CommonField.CId1;
    }
}