﻿using Android.App;
using Android.Content.PM;
using Android.OS;

using GeTuiPushDemo.ViewModels;

namespace GeTuiPushDemo
{
    [Activity(Theme = "@style/Maui.SplashTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize | ConfigChanges.Density)]
    public class MainActivity : MauiAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Com.Igexin.Sdk.PushManager.Instance.Initialize(this);

            //var viewModel = DH.Base.ServiceProvider.GetService<MainViewModel>();

            var cid = Com.Igexin.Sdk.PushManager.Instance.GetClientid(this);

            //Com.Igexin.Sdk.PushManager.Instance.RegisterPushIntentService(this, Java.Lang.Class.FromType(typeof(GeTuiPushDemo.Platforms.DemoIntentService)));

            CommonField.CId = "cid:" + cid;
            System.Diagnostics.Debug.WriteLine($"获取到的cid:{cid}");
        }
    }
}