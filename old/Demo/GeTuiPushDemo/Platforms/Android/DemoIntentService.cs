﻿using Android.App;
using Android.Content;

using Com.Igexin.Sdk;
using Com.Igexin.Sdk.Message;

using System.Diagnostics;

namespace GeTuiPushDemo.Platforms;

/// <summary>
/// 继承 GTIntentService 接收来自个推的消息，所有消息在主线程中回调，如果注册了该服务，则务必要在 AndroidManifest 中声明，否则无法接受消息
/// </summary>
//[Service(Process = ":intentservice", Exported = false)]
[Service()]
public class DemoIntentService : GTIntentService
{
    public override void OnReceiveServicePid(Context context, int pid)
    {

    }

    /// <summary>
    /// 此方法用于接收和处理透传消息。透传消息个推只传递数据，不做任何处理，客户端接收到透传消息后需要自己去做后续动作处理，如通知栏展示、弹框等。
    /// 如果开发者在客户端将透传消息创建了通知栏展示，建议将展示和点击回执上报给个推。
    /// </summary>
    /// <param name="context"></param>
    /// <param name="msg"></param>
    public override void OnReceiveMessageData(Context context, GTTransmitMessage msg)
    {

    }

    /// <summary>
    /// 接收 cid
    /// </summary>
    /// <param name="context"></param>
    /// <param name="clientid"></param>
    public override void OnReceiveClientId(Context context, String clientid)
    {
        CommonField.CId1 = $"OnReceiveClientId:{clientid}";

        System.Diagnostics.Debug.WriteLine($"获取到的cid数据IntentService：{clientid}");
    }

    /// <summary>
    /// cid 离线上线通知
    /// </summary>
    /// <param name="context"></param>
    /// <param name="online"></param>
    public override void OnReceiveOnlineState(Context context, Boolean online)
    {

    }

    /// <summary>
    /// 各种事件处理回执
    /// </summary>
    /// <param name="context"></param>
    /// <param name="cmdMessage"></param>
    public override void OnReceiveCommandResult(Context context, GTCmdMessage cmdMessage)
    {

    }

    /// <summary>
    /// 通知到达，只有个推通道下发的通知会回调此方法
    /// </summary>
    /// <param name="context"></param>
    /// <param name="msg"></param>
    public override void OnNotificationMessageArrived(Context context, GTNotificationMessage msg)
    {
    }

    /// <summary>
    /// 通知点击，只有个推通道下发的通知会回调此方法
    /// </summary>
    /// <param name="context"></param>
    /// <param name="msg"></param>
    public override void OnNotificationMessageClicked(Context context, GTNotificationMessage msg)
    {
    }
}
