﻿namespace LiteDbDemo;

public class SecureStorageService : ISecureStorageService
{ 
    public SecureStorageService()
    {

    }

    public async Task<String> Get(String key, String defaultValue = null)
    {
        var result = await SecureStorage.Default.GetAsync(key);

        if (result == null)
        {
            return defaultValue;
        }

        return result;
    }

    public async Task Save(string key, string value)
    {
        await SecureStorage.SetAsync(key, value);
    }

}
