﻿using MetroLog;

using Layout = MetroLog.Layout;

namespace DH.MetroLogSample.Layouts;

public class SimpleLayout : Layout
{
    public override string GetFormattedString(LogWriteContext context, LogEventInfo info)
    {
        return $"{info.TimeStamp:G} - {info.Level}: {info.Message}";
    }
}