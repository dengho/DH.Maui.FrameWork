﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;

using DH.WifiScan;

namespace WifiScanDemo
{
    [Activity(Theme = "@style/Maui.SplashTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize | ConfigChanges.Density)]
    public class MainActivity : MauiAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            // 初始化
            CoressWifi.Init(this);

            base.OnCreate(savedInstanceState);

            Platform.Init(this, savedInstanceState);
        }

        protected override void OnResume()
        {
            base.OnResume();

            // 恢复时重新启动NFC监听（Android 10+需要）
            CoressWifi.OnResume();
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);

            // 标签发现拦截
            CoressWifi.OnNewIntent(intent);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}