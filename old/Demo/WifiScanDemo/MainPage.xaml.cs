﻿using DH.WifiScan;
using DH.WifiScan.Model;

namespace WifiScanDemo
{
    public partial class MainPage : ContentPage
    {
        int count = 0;

        public MainPage()
        {
            InitializeComponent();

            init();
        }

        async void init()
        {
            WifiAdapter wifiAdapter = WifiAdapter.Instance;

            wifiAdapter.Init();
            wifiAdapter.InvokeResult += WifiAdapter_InvokeResult;

            await Task.CompletedTask;
        }

        private void WifiAdapter_InvokeResult(List<FormWifi> wifi)
        {
            //  throw new NotImplementedException();
        }

    }
}