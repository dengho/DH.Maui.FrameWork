﻿namespace DH.NFC;

/// <summary>
/// Interface for ITagInfo
/// </summary>
public interface ITagInfo
{
    /// <summary>
    /// Tag Raw Identifier
    /// </summary>
    byte[] Identifier { get; }

    /// <summary>
    /// Tag Serial Number
    /// </summary>
    string SerialNumber { get; }

    /// <summary>
    /// Writable tag
    /// </summary>
    bool IsWritable { get; set; }

    /// <summary>
    /// Empty tag
    /// </summary>
    bool IsEmpty { get; }

    /// <summary>
    /// Supported tag
    /// </summary>
    bool IsSupported { get; }

    /// <summary>
    /// Capacity of tag in bytes
    /// </summary>
    int Capacity { get; set; }

    /// <summary>
    /// Array of <see cref="NFCNdefRecord"/> of tag
    /// </summary>
    NFCNdefRecord[] Records { get; set; }
}

/// <summary>
/// 描述 NFC 标记中包含的信息的类
/// </summary>
public class NFCNdefRecord
{
    /// <summary>
    /// NDEF 类型
    /// </summary>
    public NFCNdefTypeFormat TypeFormat { get; set; }

    /// <summary>
    /// Mime类型用于<see cref="NFCNdefTypeFormat.Mime"/>类型
    /// </summary>
    public string MimeType { get; set; } = "text/plain";

    /// <summary>
    /// 用于<see cref="NFCNdefTypeFormat.External"/>类型的外部域
    /// </summary>
    public string ExternalDomain { get; set; }

    /// <summary>
    /// 用于<see cref="NFCNdefTypeFormat.External"/>类型的外部类型
    /// </summary>
    public string ExternalType { get; set; }

    /// <summary>
    /// 负载
    /// </summary>
    public byte[] Payload { get; set; }

    /// <summary>
    /// 网址
    /// </summary>
    public string Uri { get; set; }

    /// <summary>
    /// 字符串格式的负载
    /// </summary>
    public string Message => NFCUtils.GetMessage(TypeFormat, Payload, Uri);

    /// <summary>
    /// 两个字母ISO 639-1语言代码(例如：en、fr、de…)
    /// </summary>
    public string LanguageCode { get; set; }
}

/// <summary>
/// Enumeration of NDEF type
/// </summary>
public enum NFCNdefTypeFormat
{
    Empty = 0x00,
    WellKnown = 0x01,
    Mime = 0x02,
    Uri = 0x03,
    External = 0x04,
    Unknown = 0x05,
    Unchanged = 0x06,
    Reserved = 0x07
}