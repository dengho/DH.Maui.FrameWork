﻿namespace DH.NFC;

/// <summary>
/// <see cref="ITagInfo"/>的默认实现
/// </summary>
public class TagInfo : ITagInfo
{
    public byte[] Identifier { get; }

    /// <summary>
    /// 标签序列号
    /// </summary>
    public string SerialNumber { get; }

    /// <summary>
    /// 可写标签
    /// </summary>
    public bool IsWritable { get; set; }

    /// <summary>
    /// 标签容量（字节）
    /// </summary>
    public int Capacity { get; set; }

    /// <summary>
    /// 标记的<see cref="NFCNdefRecord"/>数组
    /// </summary>
    public NFCNdefRecord[] Records { get; set; }

    /// <summary>
    /// 空标签
    /// </summary>
    public bool IsEmpty => Records == null || Records.Length == 0 || Records[0] == null || Records[0].TypeFormat == NFCNdefTypeFormat.Empty;

    /// <summary>
    /// 是否支持
    /// </summary>
    public bool IsSupported { get; private set; }

    /// <summary>
    /// 默认构造函数
    /// </summary>
    public TagInfo()
    {
        IsSupported = true;
    }

    /// <summary>
    /// 自定义构造函数
    /// </summary>
    /// <param name="identifier">标签标识符</param>
    /// <param name="isNdef">是否Ndef标签</param>
    public TagInfo(byte[] identifier, bool isNdef = true)
    {
        Identifier = identifier;
        SerialNumber = NFCUtils.ByteArrayToHexString(identifier);
        IsSupported = isNdef;
    }

    public override string ToString() => $"TagInfo: identifier: {Identifier}, SerialNumber:{SerialNumber}, Capacity:{Capacity} bytes, IsSupported:{IsSupported}, IsEmpty:{IsEmpty}, IsWritable:{IsWritable}";
}