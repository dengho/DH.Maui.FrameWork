﻿namespace DH.Plugin.EventAggregator;

public interface IEventAggregator
{
    // 发送消息并等待在UI线程中处理后返回
    void SendMessage<T>(T message);
    // 发布消息以在UI线程中进一步处理，并立即返回。
    void PostMessage<T>(T message);
    // 注册或委派
    Action<T> RegisterHandler<T>(Action<T> eventHandler);
    // 删除代理记录
    void UnregisterHandler<T>(Action<T> eventHandler);
}