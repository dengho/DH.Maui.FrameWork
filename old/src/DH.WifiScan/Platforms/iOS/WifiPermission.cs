﻿using CoreBluetooth;

using CoreFoundation;
using System;

namespace DH.WifiScan;

public class WifiPermission : Permissions.BasePlatformPermission
{
    protected override Func<IEnumerable<string>> RequiredInfoPlistKeys
                =>
                    () => new string[] { "NSBluetoothAlwaysUsageDescription", "NSBluetoothPeripheralUsageDescription" };

    public override Task<PermissionStatus> CheckStatusAsync()
    {
        EnsureDeclared();
        return Task.FromResult(GetBleStatus());
    }

    private PermissionStatus GetBleStatus()
    {
        return PermissionStatus.Unknown;
    }
}
