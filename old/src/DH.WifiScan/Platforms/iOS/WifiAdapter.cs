﻿namespace DH.WifiScan;

public partial class WifiAdapter
{
    public static async Task<PermissionStatus> PlatformCheckAndRequestBluetoothPermission()
    {
        var status = await Permissions.CheckStatusAsync<WifiPermission>();

        status = await Permissions.RequestAsync<WifiPermission>();

        return status;
    }
}
