﻿using Android.Content;

namespace DH.WifiScan;

/// <summary>
/// Android implementation of <see cref="IWifi"/>
/// </summary>
public class WifiImplementation: IWifi
{

    /// <summary>
    /// Handle Android OnNewIntent
    /// </summary>
    /// <param name="intent">Android <see cref="Intent"/></param>
    internal void HandleNewIntent(Intent intent)
    {
        if (intent == null)
            return;
    }

    /// <summary>
    /// Handle Android OnResume
    /// </summary>
    internal void HandleOnResume()
    {

    }
}
