﻿namespace DH.WifiScan;

public class WifiPermission : Permissions.BasePlatformPermission
{
    public override (string androidPermission, bool isRuntime)[] RequiredPermissions =>
                new List<(string androidPermission, bool isRuntime)>
                {
                    (global::Android.Manifest.Permission.AccessCoarseLocation, true),
                    (global::Android.Manifest.Permission.AccessFineLocation, true),
                    (global::Android.Manifest.Permission.AccessBackgroundLocation, true),
                }.ToArray();
}
