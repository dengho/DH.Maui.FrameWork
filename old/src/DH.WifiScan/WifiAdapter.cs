﻿using DH.Base;
using DH.WifiScan.Model;

namespace DH.WifiScan;

public partial class WifiAdapter
{
    public delegate void ActionResult(List<FormWifi> wifi);

    public event ActionResult InvokeResult;

    static WifiAdapter instacne = null;

    public static WifiAdapter Instance
    {
        get
        {
            if (instacne == null)
            {
                instacne = new WifiAdapter();
            }
            return instacne;
        }
    }

    private WifiAdapter()
    {
    }

    public async void Init()
    {
        if ((await CheckAndRequestBluetoothPermission()) == PermissionStatus.Granted)
        {

        }

    }

    public static async Task<PermissionStatus> CheckAndRequestBluetoothPermission()
    {
        return await PlatformCheckAndRequestBluetoothPermission();
    }

}
