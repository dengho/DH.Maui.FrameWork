﻿namespace DH.SmartConfig;

public interface ISmartConfigContextFactory
{
    SmartConfigContext CreateContext();
}