﻿namespace DH.SmartConfig;

public interface ISmartConfigJob : IDisposable
{
    TimeSpan Timeout { get; }

    Task ExecuteAsync(SmartConfigContext context,
                      SmartConfigArguments args,
                      CancellationToken cancelToken);
}