﻿using DH.SmartConfig.Protocol;

namespace DH.SmartConfig;

public interface ISmartConfigProvider : ISmartConfigContextFactory
{
    string Name { get; }

    IProcedureEncoder CreateProcedureEncoder();

    IDevicePacketInterpreter CreateDevicePacketInterpreter();
}