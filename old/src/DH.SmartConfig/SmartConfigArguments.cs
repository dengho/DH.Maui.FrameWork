﻿using System.Net;
using System.Net.NetworkInformation;

namespace DH.SmartConfig;

public class SmartConfigArguments
{
    public string Password { get; set; } = string.Empty;
    public string Ssid { get; set; }
    public PhysicalAddress Bssid { get; set; }
    public IPAddress LocalAddress { get; set; }
    public bool? IsHiddenSsid { get; set; } = null;
}