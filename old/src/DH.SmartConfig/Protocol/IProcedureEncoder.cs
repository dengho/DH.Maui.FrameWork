﻿namespace DH.SmartConfig.Protocol;

public interface IProcedureEncoder
{
    IEnumerable<Segment> Encode(SmartConfigContext context, SmartConfigArguments args);
}