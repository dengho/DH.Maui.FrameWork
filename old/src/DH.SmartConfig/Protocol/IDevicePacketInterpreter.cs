﻿using System.Net.NetworkInformation;

namespace DH.SmartConfig.Protocol;

public interface IDevicePacketInterpreter
{
    bool Validate(SmartConfigContext context, byte[] packet);

    PhysicalAddress ParseMacAddress(byte[] packet);
}