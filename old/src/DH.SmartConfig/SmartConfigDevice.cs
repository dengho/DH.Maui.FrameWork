﻿using System.Net;
using System.Net.NetworkInformation;

namespace DH.SmartConfig;

public class SmartConfigDevice : ISmartConfigDevice
{
    public PhysicalAddress MacAddress { get; }
    public IPAddress IPAddress { get; }

    public SmartConfigDevice(PhysicalAddress mac, IPAddress ip)
    {
        this.MacAddress = mac;
        this.IPAddress = ip;
    }
}