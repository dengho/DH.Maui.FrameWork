﻿using System.Net;
using System.Net.Sockets;

namespace DH.SmartConfig.Networking;

public class DefaultDatagramClient : IDatagramClient
{
    private readonly UdpClient _udp;

    public DefaultDatagramClient()
    {
        _udp = new UdpClient(AddressFamily.InterNetwork);
        _udp.EnableBroadcast = true;
    }

    public void Bind(IPEndPoint localEndPoint)
    {
        _udp.Client.Bind(localEndPoint);
    }

    public async Task SendAsync(byte[] datagram, int bytes, IPEndPoint target)
    {
        await _udp.SendAsync(datagram, bytes, target);
    }

    public async Task<DatagramReceiveResult> ReceiveAsync()
    {
        var result = await _udp.ReceiveAsync();
        return new DatagramReceiveResult(result.Buffer, result.RemoteEndPoint);
    }

    public void Dispose()
    {
        _udp.Dispose();
    }

}