﻿using System.Net;

namespace DH.SmartConfig.Networking;

public interface IDatagramClient : IDisposable
{
    void Bind(IPEndPoint localEndPoint);
    Task SendAsync(byte[] datagram, int bytes, IPEndPoint target);

    Task<DatagramReceiveResult> ReceiveAsync();
}