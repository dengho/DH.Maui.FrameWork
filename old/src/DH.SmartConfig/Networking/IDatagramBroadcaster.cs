﻿namespace DH.SmartConfig.Networking;

public interface IDatagramBroadcaster : IDisposable
{
    Task BroadcastAsync(
        SmartConfigContext context, SmartConfigArguments args, CancellationToken cancelToken);
}