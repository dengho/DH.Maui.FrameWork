﻿using System.Net;

namespace DH.SmartConfig.Networking;

public interface IDatagramReceiver : IDisposable
{
    Task ListenAsync(
        SmartConfigContext context, IPAddress localAddress, CancellationToken cancelToken);
}