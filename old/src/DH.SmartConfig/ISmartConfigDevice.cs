﻿using System.Net;
using System.Net.NetworkInformation;

namespace DH.SmartConfig;

public interface ISmartConfigDevice
{
    PhysicalAddress MacAddress { get; }
    IPAddress IPAddress { get; }
}