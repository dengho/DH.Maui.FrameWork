﻿namespace DH.SmartConfig;

public static class SmartConfigExtensions
{
    public static async Task ExecuteAsync(
        this ISmartConfigJob self, SmartConfigContext context, SmartConfigArguments args)
    {
        await self.ExecuteAsync(context, args, CancellationToken.None);
    }
}