﻿namespace DH.SmartConfig;

public delegate void SmartConfigTimerEventHandler(object sender, SmartConfigTimerEventArgs e);