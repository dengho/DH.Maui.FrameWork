﻿namespace DH.SmartConfig.Util;

public static class LinqExtensions
{
    public static IEnumerable<IEnumerable<T>> Partition<T>(
        this IEnumerable<T> source, int partitionSize)
    {
        int i = 0;
        return source.GroupBy(x => i++ / partitionSize);
    }

}