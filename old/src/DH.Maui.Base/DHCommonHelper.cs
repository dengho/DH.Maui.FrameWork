﻿using CommunityToolkit.Maui.Alerts;
using CommunityToolkit.Maui.Core;
using CommunityToolkit.Maui.Core.Primitives;

using System.Collections;
using System.Text;
using System.Threading;

namespace DH.Base;

public static class DHCommonHelper
{
    public static String ALERT_TITLE { get; set; } = "湖北登灏";

    public static Func<Page, Task> GlobalNavigationFunction;
    public static Func<Task> PopPageFunction;

    public static void InitializeNavigation(
        Func<Page, Task> navigationFunction,
        Func<Task> popPageFunction)
    {
        GlobalNavigationFunction = navigationFunction;
        PopPageFunction = popPageFunction;
    }

    /// <summary>
    /// 弹出提示信息,约2秒左右会消失
    /// </summary>
    /// <param name="Message"></param>
    /// <returns></returns>
    public static async Task DHToast(String Message)
    {
        CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        ToastDuration duration = ToastDuration.Short;
        double fontSize = 14;

        var toast = Toast.Make(Message, duration, fontSize);

        await toast.Show(cancellationTokenSource.Token);
    }

    /// <summary>
    /// 弹出提示信息，约3.5秒左右会消失
    /// </summary>
    /// <param name="Message"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public static async Task DHToast(String Message, CancellationToken cancellationToken)
    {
        ToastDuration duration = ToastDuration.Long;
        double fontSize = 14;

        var toast = Toast.Make(Message, duration, fontSize);

        await toast.Show(cancellationToken);
    }

    /// <summary>
    /// 在调试控制台中编写调试消息
    /// </summary>
    /// <param name="message">要显示的消息</param>
    public static void Debug(string message) => System.Diagnostics.Debug.WriteLine(message);

    /// <summary>
    /// 显示消息
    /// </summary>
    /// <param name="message">要显示的消息</param>
    /// <param name="title">消息标题</param>
    /// <returns>要执行的任务</returns>
    public static Task ShowAlert(this Page page, string message, string title = null) => page.DisplayAlert(string.IsNullOrWhiteSpace(title) ? ALERT_TITLE : title, message, "OK");

    /// <summary>
    /// 十六进制转字节数组
    /// </summary>
    /// <param name="hexString"></param>
    /// <returns></returns>
    public static byte[] StrToToHexByte(string hexString)
    {
        hexString = hexString.Replace(" ", "");
        if ((hexString.Length % 2) != 0)
            hexString += " ";
        byte[] returnBytes = new byte[hexString.Length / 2];
        for (int i = 0; i < returnBytes.Length; i++)
            returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
        return returnBytes;
    }

    /// <summary>
    /// 字节数组转16进制字符串：空格分隔
    /// </summary>
    /// <param name="byteDatas"></param>
    /// <returns></returns>
    public static string ToHexStrFromByte(this byte[] byteDatas)
    {
        StringBuilder builder = new();
        for (int i = 0; i < byteDatas.Length; i++)
        {
            builder.Append(string.Format("{0:X2} ", byteDatas[i]));
        }
        return builder.ToString().Trim();
    }

    /// <summary>
    /// Bit 高底位转换,反转整个byte 中的 8位
    /// </summary>
    /// <param name="oldBit">需要转换的byte[]</param>
    /// <param name="newBit">转换到的新byte[]</param>
    /// <returns>返回反转的数据</returns>
    public static byte[] BitReverse(BitArray oldBit, byte[] newBit)
    {
        var buf = new BitArray(new byte[] { 0x00 });
        for (int i = 0; i < oldBit.Count / 8; i++)
        {
            for (int x = 0; x < 8; x++)
            {
                buf[x] = oldBit[(i * 8) + (7 - x)];
            }
            buf.CopyTo(newBit, i);
        }
        return newBit;
    }
}
