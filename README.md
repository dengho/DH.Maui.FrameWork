# DH.Maui.FrameWork

#### 介绍
基于net7 Maui的各种类库，目前主要net core 版本号为：7.0.100。

交流请加群：774046050  

另有基于Net Core的快速开发项目：[DH.FrameWork](https://gitee.com/dengho/DH.FrameWork)

目前已有的类库：  
1、NFC操作类，支持安卓和IOS  
2、个推消息推送，支持安卓  
3、事件聚合器类  
4、日志输出类