﻿namespace DH.Base.Services;

public static class DHServiceProvider {
	public static IServiceProvider? Current { get; set; }

    public static T GetService<T>() where T : notnull
		=> Current!.GetRequiredService<T>();
}
